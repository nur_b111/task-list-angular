For backend packages install, in root folder run command:
-npm i

For frontend packages install, in '/client' folder run command:
-npm i

For nower components install, in root folder run command:
-bower i

------------------------------------------------------------

For backend start, in root folder run command:
-nodemon

For frontend start, in '/client' folder run command:
-npm start