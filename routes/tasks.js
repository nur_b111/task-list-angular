var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
//var db = mongojs('mongodb+srv://Nurkassym:Zsedcx12#@cluster0-9lszy.mongodb.net/mytasklist_nurkassym?retryWrites= true');
var db = mongojs('mongodb://Nurkassym:Pomnivse1993@cluster0-shard-00-00-9lszy.mongodb.net:27017,cluster0-shard-00-01-9lszy.mongodb.net:27017,cluster0-shard-00-02-9lszy.mongodb.net:27017/mytasklist_nurkassym?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true')
     
//All 
router.get('/tasks', (req, res, next)=>{
    db.tasks.find((err, tasks)=>{
        if(err){
            res.send(err);
        }
        //console.log(tasks.length)
        res.json(tasks);
    });
});

//Get single
router.get('/task/:id', (req, res, next)=>{
    db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)},(err, task)=>{
        if(err){
            res.send(err);
        }
        //console.log(tasks.length)
        res.json(task);
    });
});

//Save Task
router.post('/task', (req, res, next)=>{
    var task = req.body;
    if(!task.title || !(task.isDone + '')){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    } else {
        db.tasks.save(task, (err, task)=>{
            if(err){
                res.send(err);
            } 
            res.json(task);
        })
    }
})

//Delete single
router.delete('/task/:id', (req, res, next)=>{
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)},(err, task)=>{
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});

//Update
router.put('/task/:id', (req, res, next)=>{
    var task = req.body;
    var updTask = {};

    if(task.isDone)
    {
        updTask.isDone = task.isDone;
    }

    if(task.title){
        updTask.title = task.title;
    }

    if(!updTask){
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.tasks.update({_id: mongojs.ObjectId(req.params.id)}, updTask, {}, (err, task)=>{
            if(err){
                res.send(err);
            }
            //console.log(tasks.length)
            res.json(task);
        });
    }
    
});

module.exports = router;